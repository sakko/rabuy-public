json.array!(@messages) do |message|
  json.extract! message, :id, :content, :latitude, :longitude, :user_id
  json.url message_url(message, format: :json)
end
